package com.cen.mybatisplus.spring1.model;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;

@Getter
@Setter
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("t_student")
public class Student {
    private int id;
    private String name;
    private int age;
    private double score;
}
