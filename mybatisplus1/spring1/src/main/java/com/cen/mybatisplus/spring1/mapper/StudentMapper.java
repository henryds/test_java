package com.cen.mybatisplus.spring1.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cen.mybatisplus.spring1.model.Student;

public interface StudentMapper extends BaseMapper<Student> {
}
