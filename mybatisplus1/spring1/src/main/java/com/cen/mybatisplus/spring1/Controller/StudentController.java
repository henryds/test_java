package com.cen.mybatisplus.spring1.Controller;

import com.cen.mybatisplus.spring1.mapper.StudentMapper;
import com.cen.mybatisplus.spring1.model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/test")
public class StudentController {
    @Autowired
    private StudentMapper studentMapper;

    @GetMapping("/list")
    public List<Student> list(){
        return studentMapper.selectList(null);
    }
}
