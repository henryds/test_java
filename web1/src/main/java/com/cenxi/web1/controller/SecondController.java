package com.cenxi.web1.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("/second")
public class SecondController {

    @RequestMapping("/test1")
    public ModelAndView test1(HttpServletRequest req, HttpServletResponse res) throws Exception {
        ModelAndView mv = new ModelAndView();
        mv.addObject("hello", "hello first spring mvc");
        mv.setViewName("first");
        return mv;
    }

    @RequestMapping("/test2")
    public String test2(){
        return "test2";
    }
}
