package com.cenxi.web1.controller;

import com.cenxi.web1.model.Student;
import org.springframework.context.ApplicationContext;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;

@Controller
@RequestMapping("/first")
public class FirstController {

    @RequestMapping("/test1")
    public ModelAndView test1(HttpServletRequest req, HttpServletResponse res) throws Exception {
        ModelAndView mv = new ModelAndView();
        mv.addObject("hello", "hello first spring mvc");
        mv.setViewName("first");
        return mv;
    }

    @RequestMapping("/test2")
    public String test2(){
        return "test2";
    }

    @RequestMapping("/test3")
    public String test3(){
        return "monkey1024";
    }

    @RequestMapping("/test4")
    @ResponseBody
    public String test4(){
        return "test4";
    }

    @RequestMapping("/test5")
    @ResponseBody
    public Student test5(){
        Student s = new Student();
        s.setAge(10);
        s.setName("中国人");
        return s;
    }
    @RequestMapping("/test6")
    public String test6(){
        int i = 1 / 0;
        return Integer.toString(i);
    }
    @RequestMapping("/test7")
    @ResponseBody
    public String test7(@DateTimeFormat(pattern = "yyyy-MM-dd") Date birthDate){
        if(birthDate == null){
            return "";
        }else{
            return (new SimpleDateFormat("yyyy-MM-dd")).format(birthDate);
        }
    }
    @RequestMapping("/test8")
    public String test8(){
        return "monkey1024";
    }
}
