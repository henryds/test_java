package com.cenxi.web1.controller.api;

import com.cenxi.web1.model.Student;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/student")
public class StudentController {
    //@RequestMapping(value="/test1",produces={"text/html; charset=UTF-8"},consumes = {"application/json; charset=UTF-8"})
    @RequestMapping(value="/test1")
    public String test1(){
        return "test1中国";
    }

    @RequestMapping("/test2")
    public Student test2(@RequestBody Student s){
        return s;
    }

    //@RequestMapping(value="/test3",produces={"application/json; charset=UTF-8"})
    @RequestMapping("/test3")
    public Student test3(){
        Student s = new Student();
        s.setAge(10);
        s.setName("中国人");
        s.setBirthDate(LocalDate.now());
        return s;
    }
    @RequestMapping(value="/test4",produces={"application/json; charset=UTF-8"})
    public Object test4(@RequestBody @Validated Student s, BindingResult br){
        if(br.hasErrors()){
            String msg = "";
            List<ObjectError> list = br.getAllErrors();
            for(ObjectError err : list){
                msg += err.getDefaultMessage() + ";";
            }
            return msg;
        }else {
            return s;
        }

    }
}
