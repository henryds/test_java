package com.cenxi.web1.model;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.time.LocalDate;


@Getter
@Setter
public class Student {
    @NotEmpty
    @Size(min=4,max=30,message = "名字长度必须在{min}和{max}之间")
    private String name;

    @Min(value = 0,message = "年龄必须大于0")
    private Integer age;

    //@JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate birthDate;
}
