package com.bben.spring1.primary.mapper;

import com.bben.spring1.primary.model.TPatient;
import com.bben.spring1.primary.vo.TPatientVo;

import java.util.List;
import java.util.Map;

public interface MyTPatientMapper extends TPatientMapper {
    List<TPatientVo> selectPatient();
    List<Map<String,Object>> selectByMap();
    List<TPatient> select2();
}
