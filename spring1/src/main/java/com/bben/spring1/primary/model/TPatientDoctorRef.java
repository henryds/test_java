package com.bben.spring1.primary.model;

import java.io.Serializable;

public class TPatientDoctorRef implements Serializable {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_patient_doctor_ref.id
     *
     * @mbg.generated Tue Mar 26 10:57:55 CST 2019
     */
    private Integer id;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_patient_doctor_ref.patient_id
     *
     * @mbg.generated Tue Mar 26 10:57:55 CST 2019
     */
    private Integer patientId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_patient_doctor_ref.doctor_id
     *
     * @mbg.generated Tue Mar 26 10:57:55 CST 2019
     */
    private Integer doctorId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table t_patient_doctor_ref
     *
     * @mbg.generated Tue Mar 26 10:57:55 CST 2019
     */
    private static final long serialVersionUID = 1L;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_patient_doctor_ref.id
     *
     * @return the value of t_patient_doctor_ref.id
     *
     * @mbg.generated Tue Mar 26 10:57:55 CST 2019
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_patient_doctor_ref.id
     *
     * @param id the value for t_patient_doctor_ref.id
     *
     * @mbg.generated Tue Mar 26 10:57:55 CST 2019
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_patient_doctor_ref.patient_id
     *
     * @return the value of t_patient_doctor_ref.patient_id
     *
     * @mbg.generated Tue Mar 26 10:57:55 CST 2019
     */
    public Integer getPatientId() {
        return patientId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_patient_doctor_ref.patient_id
     *
     * @param patientId the value for t_patient_doctor_ref.patient_id
     *
     * @mbg.generated Tue Mar 26 10:57:55 CST 2019
     */
    public void setPatientId(Integer patientId) {
        this.patientId = patientId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_patient_doctor_ref.doctor_id
     *
     * @return the value of t_patient_doctor_ref.doctor_id
     *
     * @mbg.generated Tue Mar 26 10:57:55 CST 2019
     */
    public Integer getDoctorId() {
        return doctorId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_patient_doctor_ref.doctor_id
     *
     * @param doctorId the value for t_patient_doctor_ref.doctor_id
     *
     * @mbg.generated Tue Mar 26 10:57:55 CST 2019
     */
    public void setDoctorId(Integer doctorId) {
        this.doctorId = doctorId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_patient_doctor_ref
     *
     * @mbg.generated Tue Mar 26 10:57:55 CST 2019
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", patientId=").append(patientId);
        sb.append(", doctorId=").append(doctorId);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}