package com.bben.spring1.primary.vo;

import com.bben.spring1.primary.model.TDoctor;
import com.bben.spring1.primary.model.TNurse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TPatientVo {
    private int id;
    private String name;
    private List<TDoctor> doctorList;
    private List<TNurse> nurseList;
}
