package com.bben.spring1.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IndexController {
    @RequestMapping("/")
    public String index(){
        return "index";
    }
    //@RequestMapping("/error")
    public String error(){
        return "error";
    }
}
