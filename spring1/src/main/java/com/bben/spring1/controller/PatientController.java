package com.bben.spring1.controller;

import com.bben.spring1.primary.mapper.MyTPatientMapper;
import com.bben.spring1.primary.model.TPatient;
import com.bben.spring1.primary.vo.TPatientVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/patient")
public class PatientController {
    @Autowired
    private MyTPatientMapper myTPatientMapper;

    @RequestMapping("/list")
    public List<TPatientVo> list(){
        return myTPatientMapper.selectPatient();
    }

    @RequestMapping("/map")
    public List<Map<String,Object>> map(){
        return myTPatientMapper.selectByMap();
    }

    @RequestMapping("/list2")
    public List<TPatient> list2(){
        return myTPatientMapper.selectByExample(null);
    }

    @RequestMapping("/select2")
    public List<TPatient> select2(){
        return myTPatientMapper.select2();
    }
}
