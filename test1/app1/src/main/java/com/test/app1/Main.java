package com.test.app1;

import org.apache.commons.beanutils.BeanUtils;

public class Main {
    public static void main(String[] args) {
        Student s = new Student();
        try {
            BeanUtils.setProperty(s, "name", "Henry");
            BeanUtils.setProperty(s,"age",11);
        }catch (Exception e){
            e.printStackTrace();
        }
        System.out.printf(s.toString());
    }
}
