package com.jzr.test2.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

@RestController
@RequestMapping("/test")
public class TestController {
    @GetMapping("/test1")
    public String test1(){
        return "test1";
    }

    @GetMapping("/test2")
    public Object test2() throws SQLException, ClassNotFoundException {
        String usr = "admin";
        String pwd = "123456";
        String url =  "jdbc:Cache://localhost:1972/SAMPLES";

        Class.forName("com.intersys.jdbc.CacheDriver");
        Connection conn = DriverManager.getConnection(url, usr, pwd);

        Statement stmt = conn.createStatement();

        java.sql.ResultSet rs = stmt.executeQuery("select * from test.test1");

        rs.next();
        return rs.getString("id") + " "
                + rs.getString("companyId")
                + rs.getString("docNo");

    }
}
