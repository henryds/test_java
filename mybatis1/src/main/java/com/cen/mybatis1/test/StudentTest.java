package com.cen.mybatis1.test;

import com.cen.mybatis1.bean.Student;
import com.cen.mybatis1.dao.StudentDao;
import com.cen.mybatis1.util.MyBatisUtil;
import org.apache.ibatis.session.SqlSession;
import org.junit.jupiter.api.Test;

import java.util.List;

public class StudentTest {
    @Test
    public void insert(){
        Student student = new Student();
        student.setName("henry");
        student.setAge(1);
        student.setScore(1.1);

        try(SqlSession sqlSession = MyBatisUtil.getSqlSession()) {
            StudentDao studentDao = sqlSession.getMapper(StudentDao.class);
            studentDao.insert(student);
            System.out.println(student);
        };
    }

    @Test
    public void list(){
        try(SqlSession sqlSession = MyBatisUtil.getSqlSession()) {
            StudentDao studentDao = sqlSession.getMapper(StudentDao.class);
            List<Student> list = studentDao.list();
            list.forEach(e -> System.out.println(e));
        }
    }
}
