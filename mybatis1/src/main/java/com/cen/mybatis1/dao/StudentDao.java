package com.cen.mybatis1.dao;

import com.cen.mybatis1.bean.Student;

import java.util.List;

public interface StudentDao {
    void insert(Student student);
    List<Student> list();
}
