package com.cen.mybatis1.bean;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@ToString
@NoArgsConstructor
public class Student {
    private int id;
    private String name;
    private int age;
    private double score;
}
