package com.cenxi.study.mybatis1.test;

import com.cenxi.study.mybatis1.model.Student;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import javax.annotation.Resource;
import java.io.Reader;
import java.util.List;

public class Test1 {
    public static SqlSessionFactory getSessionFactory() throws Exception{
        Reader reader = Resources.getResourceAsReader("mybatis.config");
        SqlSessionFactory sessionFactory = new SqlSessionFactoryBuilder().build(reader);
        return sessionFactory;
    }

    public static void add() throws Exception{
        SqlSessionFactory sessionFactory = getSessionFactory();
        try(SqlSession session = sessionFactory.openSession()){
            String statement = "com.cenxi.study.mybatis1.mapper.StudentMapper.add";
            Student student = new Student("Henry4",20,55.5);
            int count = session.insert(statement,student);
            session.commit();
            System.out.println(count);
        }
    }
    public static void remove() throws Exception{
        SqlSessionFactory sessionFactory = getSessionFactory();
        try(SqlSession session = sessionFactory.openSession()){
            String statement = "com.cenxi.study.mybatis1.mapper.StudentMapper.remove";
            int count = session.delete(statement,23);
            session.commit();
            System.out.println(count);
        }
    }
    public static void update() throws Exception{
        SqlSessionFactory sessionFactory = getSessionFactory();
        try(SqlSession session = sessionFactory.openSession()){
            String statement = "com.cenxi.study.mybatis1.mapper.StudentMapper.update";
            Student student = new Student();
            student.setId(22);
            student.setName("henry22;delete from t_student;)");
            student.setAge(38);
            student.setScore(66.4);
            int count = session.update(statement,student);
            session.commit();
            System.out.println(count);
        }
    }

    public static void list() throws Exception{
        SqlSessionFactory sessionFactory = getSessionFactory();
        try(SqlSession session = sessionFactory.openSession()){
            String statement = "com.cenxi.study.mybatis1.mapper.StudentMapper.list";
            List<Student> list = session.selectList(statement,"name");
            System.out.println(list);
        }
    }

    public static void main(String[] args) throws Exception {
        //add();
        list();
        //update();
        //list();
    }
}
