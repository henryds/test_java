package com.cenxi.study.mybatis1.convert;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class BooleanAndIntHandler extends BaseTypeHandler<Boolean> {

    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, Boolean aBoolean, JdbcType jdbcType) throws SQLException {
        ps.setInt(i,aBoolean ? 1 : 0);
    }

    @Override
    public Boolean getNullableResult(ResultSet rs, String s) throws SQLException {
        Integer value =  rs.getInt(s);
        return value == null || value.intValue()==0 ? false : true;
    }

    @Override
    public Boolean getNullableResult(ResultSet rs, int i) throws SQLException {
        Integer value =  rs.getInt(i);
        return value == null || value.intValue()==0 ? false : true;
    }

    @Override
    public Boolean getNullableResult(CallableStatement cs, int i) throws SQLException {
        Integer value =  cs.getInt(i);
        return value == null || value.intValue()==0 ? false : true;
    }
}
