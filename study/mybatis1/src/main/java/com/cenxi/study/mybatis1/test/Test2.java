package com.cenxi.study.mybatis1.test;

import com.cenxi.study.mybatis1.mapper.StudentMapper;
import com.cenxi.study.mybatis1.model.Student;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.Reader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Test2 {
    public static SqlSessionFactory getSessionFactory() throws Exception{
        Reader reader = Resources.getResourceAsReader("mybatis.config");
        SqlSessionFactory sessionFactory = new SqlSessionFactoryBuilder().build(reader);
        return sessionFactory;
    }

    public static void add() throws Exception{
        SqlSessionFactory sessionFactory = getSessionFactory();
        try(SqlSession session = sessionFactory.openSession()){
            Student student = new Student("Henry4",20,55.5);
            StudentMapper mapper = session.getMapper(StudentMapper.class);
            mapper.add(student);
            session.commit();
            System.out.println("add");
        }
    }
    public static void remove() throws Exception{
        SqlSessionFactory sessionFactory = getSessionFactory();
        try(SqlSession session = sessionFactory.openSession()){
            StudentMapper mapper = session.getMapper(StudentMapper.class);
            mapper.remove(25);
            session.commit();
            System.out.println("remove");
        }
    }
    public static void update() throws Exception{
        SqlSessionFactory sessionFactory = getSessionFactory();
        try(SqlSession session = sessionFactory.openSession()){
            Student student = new Student();
            student.setId(22);
            student.setName("henry22x)");
            student.setAge(38);
            student.setScore(66.4);
            StudentMapper mapper = session.getMapper(StudentMapper.class);
            mapper.update(student);
            session.commit();
            System.out.println("update");
        }
    }

    public static void list() throws Exception{
        SqlSessionFactory sessionFactory = getSessionFactory();
        try(SqlSession session = sessionFactory.openSession()){
            StudentMapper mapper = session.getMapper(StudentMapper.class);
            List<Student> list = mapper.list("name");
            System.out.println(list);
        }
    }

    public static void spCount() throws Exception{
        SqlSessionFactory sessionFactory = getSessionFactory();
        try(SqlSession session = sessionFactory.openSession()){
            StudentMapper mapper = session.getMapper(StudentMapper.class);
            Map<String,Object> map = new HashMap<>();
            map.put("pName","%henry%");
            mapper.spCount(map);
            int count = (int)map.get("pCount");
            System.out.println(count);
        }
    }
    public static void spDelete() throws Exception{
        SqlSessionFactory sessionFactory = getSessionFactory();
        try(SqlSession session = sessionFactory.openSession()){
            StudentMapper mapper = session.getMapper(StudentMapper.class);
            Map<String,Object> map = new HashMap<>();
            map.put("id",24);
            mapper.spDelete(map);
            session.commit();
            System.out.println("spDelete");
        }
    }
    public static void main(String[] args) throws Exception {
        //add();
//        list()
//        remove();
//        update();
//        list();
//        spCount();
        list();
        //spDelete();
    }
}
