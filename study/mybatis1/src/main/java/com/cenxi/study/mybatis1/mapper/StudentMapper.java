package com.cenxi.study.mybatis1.mapper;

import com.cenxi.study.mybatis1.model.Student;

import java.util.List;
import java.util.Map;

public interface StudentMapper {
    void add(Student student);
    void remove(int id);
    int update(Student student);
    void spCount(Map<String,Object> map);
    List<Student> list(String orderByClause);
    void spDelete(Map<String,Object> map);
}
