<%--
  Created by IntelliJ IDEA.
  User: henry
  Date: 2019/10/9
  Time: 14:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html>
<head>
    <title>test1</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
</head>
<body>
    <fmt:message key="resource.confirm"/>
    <fmt:message key="resource.exit"/>
    ${requestScope.student.id} - ${requestScope.student.name} - ${requestScope.student.age} <br/>
    ${sessionScope.student.id} - ${sessionScope.student.name} - ${sessionScope.student.age}
    测试
</body>
</html>
