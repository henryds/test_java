package com.cenxi.mvc1.converter;

import com.cenxi.mvc1.entity.Student;
import org.springframework.core.convert.converter.Converter;

public class String2Student implements Converter<String, Student> {

    @Override
    public Student convert(String s) {
        String[] arr = s.split("-");
        Student student = new Student();
        student.setId(Integer.parseInt(arr[0]));
        student.setName(arr[1]);
        student.setAge(Integer.parseInt(arr[2]));
        return student;
    }
}
