package com.cenxi.mvc1.controller;

import com.cenxi.mvc1.entity.Student;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("test")
public class TestController {

    @RequestMapping("test1")
    public String test1(Map<String,Student> map){
        map.put("student",new Student(1,"ww",23));
        System.out.println("test1");
        return "test1"; //返回视图名；不包括前缀和后缀
    }

    @RequestMapping("test2")
    public String test2(@RequestParam("stuInfo") Student student, Map<String,Student> map){
        map.put("student",student);
        return "test1";
    }

    @RequestMapping("test3/{id}")
    public String test3(@PathVariable Integer id,Map<String,Object> map){
        Student student = new Student();
        student.setId(id);
        student.setName("中国人");
        student.setAge(id);
        map.put("student",student);
        return "test1";
    }

    @RequestMapping("test4")
    public String test4(Integer id, @RequestParam("stuName") String name,Integer age,Map<String,Object> map){
        Student student = new Student();
        student.setId(id);
        student.setName(name);
        student.setAge(age);
        map.put("student",student);
        return "test1";
    }

    @RequestMapping("test5")
    @ResponseBody
    public String test5(@RequestBody String s){
        return s;
    }

    @RequestMapping("test6")
    @ResponseBody
    public String test6(@CookieValue("JSESSIONID") String sessionId){
        return sessionId;
    }

    @RequestMapping("test7")
    @ResponseBody
    public String test7(@RequestHeader("Content-Type") String cType){
        return cType;

    }

    @RequestMapping("test8")
    @ResponseBody
    public Student test8(@Validated @RequestBody Student student, BindingResult br){
        if(br.hasErrors()){
            List<ObjectError> listError = br.getAllErrors();
            for(ObjectError err : listError){
                System.out.println(err.getDefaultMessage());
            }
        }
        return student;
    }

    //@RequestMapping(value = "test9",produces = {"text/plain;charset=UTF-8"})
    @RequestMapping(value = "test9")
    @ResponseBody
    public String test9(@RequestParam("file")MultipartFile uploadFile) throws IOException {
        if(!uploadFile.isEmpty()){
            File file = new File("d:\\" + uploadFile.getOriginalFilename());
            uploadFile.transferTo(file);
        }
        return "上传成功";
    }

    //@RequestMapping(value = "test10",produces = {"application/json;charset=UTF-8"})
    @RequestMapping(value = "test10")
    @ResponseBody
    public Student test10(@RequestBody Student student){
        return student;
    }

    @RequestMapping(value = "test11")
    @ResponseBody
    public String test11(){
        return "中国人";
    }
}
