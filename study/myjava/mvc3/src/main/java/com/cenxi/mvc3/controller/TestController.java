package com.cenxi.mvc3.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/test")
public class TestController {
    @RequestMapping("test1")
    public ModelAndView test1(){
        ModelAndView mv = new ModelAndView();
        mv.setViewName("test1");
        return  mv;
    }
}
