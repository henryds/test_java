package container;

import java.util.Arrays;

public class MyQueue {
    private int[] data;

    public MyQueue(){
        data = new int[0];
    }
    public void add(int e){
        int[] tmp = new int[data.length+1];
        for(int i=0;i<data.length;i++){
            tmp[i] = data[i];
        }
        tmp[data.length] = e;
        data = tmp;
    }
    public int poll() throws Exception{
        if(data.length == 0){
            throw new Exception("没有数据");
        }
        int first = data[0];
        int[] tmp = new int[data.length-1];
        for(int i=1;i<data.length;i++){
            tmp[i-1] = data[i];
        }
        data = tmp;
        return first;
    }
    public void show(){
        System.out.println(Arrays.toString(data));
    }

    public static void main(String[] args) throws Exception {
        MyQueue q = new MyQueue();
        int size = 20;
        for(int i=0;i<size;i++){
            q.add(i);
        }
        q.show();
        for(int i=0;i<size;i++){
            System.out.println(q.poll());
        }
        q.show();

    }
}
