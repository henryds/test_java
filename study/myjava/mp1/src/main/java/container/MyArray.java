package container;

import com.sun.javafx.logging.PulseLogger;

import java.util.Arrays;

public class MyArray {
    private int data[];
    private int size;
    private int capacity;
    private static final int INIT_CAPACITY = 32;

    public MyArray(){
        size = 0;
        capacity = INIT_CAPACITY;
        data = new int[capacity];
    }
    public MyArray(int capacity){
        size = 0;
        this.capacity = capacity;
        data = new int[capacity];
    }
    public int size(){
        return size;
    }
    public void show(){
        System.out.println(Arrays.toString(this.toArray()));
    }
    public MyArray add(int t){
        ensureCapacity(size+1);
        data[size++] = t;
        return this;
    }
    public MyArray remove(int index){
        if(index<0 && index>=size){
            throw new RuntimeException("下标越界");
        }
        for(int i=index;i<size;i++){
            data[i] = data[i+1];
        }
        size--;

        return this;
    }
    public int get(int index){
        if(index<0 || index>=size){
            throw new RuntimeException("下标越界");
        }
        return data[index];
    }
//    public MyArray insert(int index, int element){
//        if(index<0 && index>=data.length){
//            throw new RuntimeException("下标越界");
//        }
//        int[] tmp = new int[data.length+1];
//        for(int i=0;i<index;i++){
//            tmp[i] = data[i];
//        }
//        tmp[index] = element;
//        for(int i=index;i<data.length;i++){
//            tmp[i+1] = data[i];
//        }
//        data = tmp;
//        return this;
//    }
    public MyArray insert(int index, int element){
        if(index<0 || index>=data.length){
            throw new RuntimeException("下标越界");
        }
        ensureCapacity(size+1);
        for(int i=size;i>index;i--){
            data[i] = data[i-1];
        }
        data[index] = element;
        return  this;
    }
    public MyArray set(int index, int element){
        if(index<0 || index>=data.length){
            throw new RuntimeException("下标越界");
        }
        data[index] = element;
        return this;
    }
    public MyArray set(int[] pData){
        ensureCapacity(pData.length);
        for(int i=0;i<pData.length;i++){
            data[i] = pData[i];
        }
        size = pData.length;
        return this;
    }
    public int indexOf(int element){
        for(int i=0;i<size;i++){
            if(data[i] == element){
                return i;
            }
        }
        return -1;
    }
    public int find2(int e){
        int start = 0;
        int end = size-1;
        int mid = (start+end)/2;
        while(true) {
            if(e == data[mid]){
                return mid;
            }else if (e > data[mid]) {
                start = mid + 1;
            } else {
                end = mid - 1;
            }

            if(start>=end){
                return -1;
            }

            mid = (start+end)/2;
        }
    }

    public int[] toArray(){
        int[] tmp = new int[size];
        for(int i=0;i<size;i++){
            tmp[i] = data[i];
        }
        return tmp;
    }

    private void ensureCapacity(int expCapacity){
        if(expCapacity > capacity){
            if(1.5*capacity > expCapacity){
                expCapacity = ((int)(1.5*capacity))+1;
            }
            int[] tmp = new int[expCapacity];
            for(int i=0;i<size;i++){
                tmp[i] = data[i];
            }
            data = tmp;
        }
    }

    //冒泡排序
    public static void bubbleSort(int[] a){
        for(int i=0;i<a.length;i++){
            for(int j=0;j<a.length-1-i;j++){
                if(a[j]>a[j+1]){
                    int tmp = a[j];
                    a[j]=a[j+1];
                    a[j+1]=tmp;
                }
            }
        }
    }
    //快速排序
    public static void quickSort(int[] a, int start, int end){
        //递归终止条件
        if(start < end) {
            int stand = a[start]; //基准数，以这个基准数为分割线，比这个基准数小的放左边，大的放右边
            int low = start;
            int high = end;
            while (low < high) {
                //右边比基准数大
                while (low < high && stand <= a[high]) {
                    high--;
                }
                //如果小放左边
                a[low] = a[high];

                //左边比基准数小
                while (low < high && a[low] <= stand) {
                    low++;
                }
                //如果大放右边
                a[high] = a[low];
            }
            //基准数放在低位
            a[low] = stand;
            //处理左边
            quickSort(a, start, low);
            //处理右边
            quickSort(a, low + 1, end);
        }
    }
    //插入排序
    public static void insertSort(int[] a){
        //从第二元素开始
        for(int i=1;i<a.length-1;i++){
            //如果比前一个小
            if(a[i] < a[i-1]){
                int tmp = a[i];  //保存到临时变量
                //和前面所有元素比较，直到比较比它小的为止
                int j;
                for(j=i-1;j>=0 && a[j]>tmp;j--){
                    a[j+1] = a[j]; //大的往后移动
                }
                a[j+1] = tmp; //临时变量放回到上个循环的最后一个元素
            }
        }
    }
    //希尔排序
    public static void shellSort(int[] a){
        //遍历所有的步长
        for(int d=a.length/2;d>0;d/=2){
            //遍历所有元素
            for(int i=d;i<a.length;i++){
                //遍历本组所有元素
                for(int j=i-d;j>=0;j-=d){
                    if(a[j] > a[j+d]){
                        int tmp = a[j];
                        a[j] = a[j+d];
                        a[j+d] = tmp;
                    }
                }
            }
        }
    }
    //选择排序
    public static void selectSort(int[] a){
        for(int i=0;i<a.length;i++){
            int minIdx = i;
            //选择最小值的下标
            for(int j=i+1;j<a.length;j++){
                if(a[minIdx] > a[j]){
                    minIdx = j;
                }
            }
            //如果i位置不是最小值，就交换
            if(i != minIdx){
                int tmp = a[i];
                a[i] = a[minIdx];
                a[minIdx] = tmp;
            }
        }
    }
    public static void main(String[] args) {
        int[] a = {2,7,3,9,0,1,4,6,5,8};
        //MyArray.bubbleSort(a);
        //MyArray.quickSort(a,0,a.length-1);
        //MyArray.insertSort(a);
        //MyArray.shellSort(a);
        MyArray.selectSort(a);
        System.out.println(Arrays.toString(a));
    }
}
