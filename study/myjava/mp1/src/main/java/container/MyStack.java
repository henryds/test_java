package container;

import java.util.Arrays;

public class MyStack {
    private int[] data;
    private int size;
    private int capacity;
    private static final int INIT_CAPACITY = 32;

    public MyStack(){
        size = 0;
        capacity = INIT_CAPACITY;
        data = new int[capacity];
    }
    public int size(){
        return size;
    }
    public boolean isEmpty(){
        return size == 0;
    }
    public MyStack(int capacity){
        size = 0;
        this.capacity = capacity;
        data = new int[capacity];
    }

    public MyStack push(int element){
        ensureCapacity(size+1);
        data[size++] = element;
        return  this;
    }
    public void show(){
        System.out.println(Arrays.toString(this.toArray()));
    }
    public int pop() throws Exception{
        if(isEmpty()){
            throw new Exception("已经没有数据");
        }
        return data[--size];
    }
    public int[] toArray(){
        int[] tmp = new int[size];
        for(int i=0;i<size;i++){
            tmp[i] = data[i];
        }
        return tmp;
    }
    public int peek() throws Exception{
        if(isEmpty()){
            throw new Exception("已经没有数据");
        }
        return data[size-1];
    }

    private void ensureCapacity(int expCapacity){
        if(expCapacity > capacity){
            if(1.5*capacity > expCapacity){
                expCapacity = ((int)(1.5*capacity))+1;
            }
            int[] tmp = new int[expCapacity];
            for(int i=0;i<size;i++){
                tmp[i] = data[i];
            }
            data = tmp;
        }
    }


    public static void main(String[] args) throws Exception {
        MyStack s = new MyStack();
        s.push(1);
        s.push(2);
        s.show();
        System.out.println(s.pop());
        //System.out.println(s.pop());
        //System.out.println(s.pop());
        s.show();
        System.out.println(s.peek());
        System.out.println(s.peek());
        System.out.println(s.peek());
        System.out.println(s.peek());
        s.show();

    }
}
