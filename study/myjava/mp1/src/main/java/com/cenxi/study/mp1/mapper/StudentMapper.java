package com.cenxi.study.mp1.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cenxi.study.mp1.model.Student;

public interface StudentMapper extends BaseMapper<Student> {

}
