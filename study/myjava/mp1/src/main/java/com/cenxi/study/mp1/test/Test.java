package com.cenxi.study.mp1.test;

import com.cenxi.study.mp1.mapper.StudentMapper;
import com.cenxi.study.mp1.model.Student;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

public class Test {
    public static <T> T getMapper(Class<T> tClass){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        T mapper = context.getBean(tClass);
        return mapper;
    }
    public static void insert(){
        StudentMapper studentMapper = getMapper(StudentMapper.class);
        Student student = new Student("Henry5",5);
        int count = studentMapper.insert(student);
        System.out.println(count);
    }
    public static void list(){
        StudentMapper studentMapper = getMapper(StudentMapper.class);
        List<Student> list = studentMapper.selectList(null);
        System.out.println(list);
    }
    public static void main(String[] args) {
//        insert();
        list();
    }
}
