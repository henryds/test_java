package com.cenxi.study.mp1.model;

public class Person {
    private int id;
    private String name;

    public Person(){

    }
    public Person(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public static void show(Person[] persons){
        for(Person p : persons){
            System.out.println(p);
        }
    }
    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    public static void main(String[] args) {
        print(10);
    }
    private static void print(int i){
        if(i>0){
            System.out.println(i);
            print(i-1);
        }

    }
}
